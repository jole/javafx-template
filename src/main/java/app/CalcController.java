package app;

import java.util.List;
import java.util.function.BinaryOperator;
import java.util.function.UnaryOperator;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.Labeled;
import javafx.scene.control.ListView;

public class CalcController {

    private Calc calc;

    public CalcController() {
        calc = new Calc(0.0, 0.0, 0.0);
    }

    public Calc getCalc() {
        return calc;
    }

    public void setCalc(Calc calc) {
        this.calc = calc;
        updateOperandsView();
    }

    @FXML
    private ListView<Double> operandsView;

    @FXML
    private Label operandView;

    @FXML
    void initialize() {
        updateOperandsView();
    }

    private void updateOperandsView() {
        List<Double> operands = operandsView.getItems();
        operands.clear();
        int elementCount = Math.min(calc.getOperandCount(), 3);
        for (int i = 0; i < elementCount; i++) {
            operands.add(calc.peekOperand(elementCount - i - 1));
        }
    }

    private String getOperandString() {
        return operandView.getText();
    }

    private boolean hasOperand() {
        return !getOperandString().isBlank();
    }

    private double getOperand() {
        return Double.valueOf(operandView.getText());
    }

    private void setOperand(String operandString) {
        operandView.setText(operandString);
    }

    @FXML
    void handleEnter() {
        if (hasOperand()) {
            calc.pushOperand(getOperand());
        } else {
            calc.dup();
        }
        setOperand("");
        updateOperandsView();
    }

    private void appendToOperand(String s) {
        setOperand(getOperandString() + s);
    }

    @FXML
    void handleDigit(ActionEvent ae) {
        if (ae.getSource() instanceof Labeled l) {
            appendToOperand(l.getText());
            updateOperandsView();
        }
    }

    @FXML
    void handlePi() {
        calc.pushOperand(Math.PI);
        updateOperandsView();
    }

    @FXML
    void handlePoint() {
        var operandString = getOperandString();
        if (operandString.contains(".")) {
            int i = operandString.indexOf('.') + 1;
            setOperand(operandString.substring(0, i));
        } else {
            appendToOperand(".");
            updateOperandsView();
        }
    }

    @FXML
    void handleClear() {
        setCalc(new Calc(0.0, 0.0, 0.0));
        updateOperandsView();
    }

    @FXML
    void handleSwap() {
        getCalc().swap();
        updateOperandsView();
    }

    private void performOperation(UnaryOperator<Double> op) {
        getCalc().performOperation(op);
        updateOperandsView();
    }

    private void performOperation(boolean swap, BinaryOperator<Double> op) {
        if (hasOperand()) {
            calc.pushOperand(getOperand());
            setOperand("");
        }
        if (swap) {
            getCalc().swap();
        }
        getCalc().performOperation(op);
        updateOperandsView();
    }

    @FXML
    void handleOpAdd() {
        performOperation(false, (a, b) -> a + b);
    }

    @FXML
    void handleOpSub() {
        performOperation(false, (a, b) -> a - b);
    }

    @FXML
    void handleOpMult() {
        performOperation(false, (a, b) -> a * b);
    }

    @FXML
    void handleOpDiv() {
        performOperation(false, (a, b) -> a / b);
    }

    @FXML
    void handleSqrt() {
        performOperation(a -> Math.sqrt(a));
    }
}
